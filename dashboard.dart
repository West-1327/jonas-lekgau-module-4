import 'package:flutter/material.dart';
import 'main.dart';
//import 'main.dart';
import 'layouts.dart';

class DashBoard extends StatefulWidget {
  const DashBoard({Key? key}) : super(key: key);

  @override
  State<DashBoard> createState() => _DashBoardState();
}

class _DashBoardState extends State<DashBoard> {
  void click() {
    //this.name = controller.text;
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => FirstScreen()));
  }

  void onclick() {
    //this.name = controller.text;
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => SecondScreen()));
  }

  void toclick() {
    //this.name = controller.text;
    Navigator.push(context, MaterialPageRoute(builder: (context) => Profile()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("Dashboard Navigation")),
        body: Column(
          children: <Widget>[
            Container(
                height: 50,
                padding: const EdgeInsets.fromLTRB(150, 0, 50, 0),
                child: ElevatedButton(
                  child: const Text('First Screen'),
                  onPressed: () {
                    this.click();
                  },
                )),
            Container(
                height: 50,
                padding: const EdgeInsets.fromLTRB(150, 0, 50, 0),
                child: ElevatedButton(
                  child: const Text('Second Screen'),
                  onPressed: () {
                    this.onclick();
                  },
                )),
            Container(
                height: 50,
                padding: const EdgeInsets.fromLTRB(150, 0, 50, 0),
                child: ElevatedButton(
                  child: const Text('Profile Edit'),
                  onPressed: () {
                    this.toclick();
                  },
                )),
            Container(
                height: 50,
                padding: const EdgeInsets.fromLTRB(150, 0, 50, 0),
                child: ElevatedButton(
                  child: const Text('LogOut'),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => MyApp()));
                  },
                )),
          ],
        ));
  }
}

class FirstScreen extends StatelessWidget {
  const FirstScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Second Screen'),
      ),
      body: Center(
        child: ElevatedButton(
          // Within the SecondScreen widget
          onPressed: () {
            // Navigate back to the first screen by popping the current route
            // off the stack.
            Navigator.pop(context);
          },
          child: const Text('Go back!'),
        ),
      ),
    );
  }
}
